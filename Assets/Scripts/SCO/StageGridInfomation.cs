﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "new Stage Grid Info", menuName = "Data/Grid", order = 1)]
public class StageGridInfomation : SerializedScriptableObject
{
    public GridNode gridNode;
}


[System.Serializable]
public class GridNode 
{
    [SerializeReference] public Dictionary<Vector2Int, Node> nodesDic;
    int countX = 0;
    int countY = 0;
    public GridNode(int indexX, int indexY)
    {
        nodesDic = new Dictionary<Vector2Int, Node>();
    }

    public Node this[int indexX, int indexY]
    {
        get
        {

            if (nodesDic != null
                && nodesDic.ContainsKey(new Vector2Int(indexX, indexY)))
                //&& nodesDic[indexX].nodesDic != null
                //&& nodesDic[indexX].nodesDic.ContainsKey(indexY))
            {
                return nodesDic[new Vector2Int(indexX, indexY)];
            }
            else
            {
                return null;
            }
        }
        set => nodesDic[new Vector2Int(indexX, indexY)] = value;
    }


    //노드 추가
    public void AddNode(int indexX, int indexY, Node node)
    {
        var index = new Vector2Int(indexX, indexY);
        if (!nodesDic.ContainsKey(index))
        {
            nodesDic.Add(index, node);
            if (countX < indexX)
            {
                countX = indexX + 2;
            }
            if (countY < indexY)
            {
                countY = indexY + 2;
            }
        }
        else
        {
            Debug.LogError($"({indexX}, {indexY})에는 이미 타일이 존재합니다.");
        }
    }

    public int GetLength(int index)
    {
        switch (index)
        {
            case 0:
                return countX;
            case 1:
                return countY;
            default:
                return -1;
        }
    }

    public void ConnectNode()
    {
        for (int i = 0; i < countX; i++)
        {
            for (int j = 0; j < countY; j++)
            {
                if (NodeNullable(i,j))
                {
                    //T
                    if (NodeNullable(i, j + 1))
                    {
                        this[i, j].neighborNode[(int)DirEight.T] = this[i, j + 1];
                    }

                    //LT
                    if (NodeNullable(i-1, j + 1))
                    {
                        this[i, j].neighborNode[(int)DirEight.LT] = this[i -1 , j + 1];
                    }

                    //L
                    if (NodeNullable(i - 1, j))
                    {

                        //Debug.Log($"({i}, {j} : {this[i - 1, j].index}");

                        this[i, j].neighborNode[(int)DirEight.L] = this[i - 1, j];
                    }

                    //LB
                    if (NodeNullable(i - 1, j -1))
                    {

                        //Debug.Log($"({i}, {j} : {this[i - 1, j].index}");

                        this[i, j].neighborNode[(int)DirEight.LB] = this[i - 1, j -1];
                    }

                    //B
                    if (NodeNullable(i, j - 1))
                    {
                        this[i, j].neighborNode[(int)DirEight.B] = this[i, j - 1];
                    }

                    //RB
                    if (NodeNullable(i + 1, j - 1))
                    {
                        this[i, j].neighborNode[(int)DirEight.RB] = this[i + 1, j - 1];
                    }

                    //R
                    if (NodeNullable(i + 1, j))
                    {
                        this[i, j].neighborNode[(int)DirEight.R] = this[i + 1, j];
                    }

                    //RT
                    if (NodeNullable(i + 1, j + 1))
                    {
                        this[i, j].neighborNode[(int)DirEight.RT] = this[i + 1, j + 1];
                    }


                }
            }
        }
    }

    public bool NodeNullable(int i, int j) {
        return this[i, j] != null && this[i, j].tileType != TileType.Unmoveable;
    }
}

[System.Serializable]
public class Nodes
{
    [SerializeReference] public Dictionary<int, Node> nodesDic;
    public Nodes()
    {
        nodesDic = new Dictionary<int, Node>();
    }
}

[System.Serializable]
public class Node
{
    public Node[] neighborNode;

    public Vector2Int index;
    public Vector3 position;
    public Vector3 positionLT;
    public Vector3 positionLB;
    public Vector3 positionRT;
    public Vector3 positionRB;
    public Quaternion rotation;
    public TileType tileType;

    public Node(Vector2Int _index, Vector3 position, Quaternion rotation)
    {
        index = _index;
        this.position = position;
        this.rotation = rotation;
        this.tileType = TileType.Unmoveable;
        neighborNode = new Node[8];
    }

    public Node(Vector2Int _index) {
        index = _index;
        this.tileType = TileType.Unmoveable;
        neighborNode = new Node[8];
    }
}

public enum TileType
{
    Moveable_Plane,
    Unmoveable,
    Moveable_Slope_LR,
    Moveable_Slope_RL,
    Moveable_Slope_TB,
    Moveable_Slope_BT,
    Moveable_Hill_LR,
    Moveable_Hill_RL,
    Moveable_Hill_TB,
    Moveable_Hill_BT,
}
