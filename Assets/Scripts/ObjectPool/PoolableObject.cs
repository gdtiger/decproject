﻿using UnityEngine;
using DG.Tweening;

public class PoolableObject : MonoBehaviour {
    public ObjectPoolSCO objectPool;
    public int prefabIndex;
    public PrefabID prefabID;

    [SerializeField] bool hasDeadTime;
    [SerializeField] float deadTime;
    public float DeadTime { get { return deadTime; } set { deadTime = value; } }

    public virtual void OnEnable()
    {
        if (hasDeadTime)
        {
            Invoke("Return", deadTime);//this will happen after 2 seconds
        }
    }



    public virtual void Return()
    {
        if (objectPool == null)
        {
            objectPool = GameManager.Instance.objectPool;
        }
        Debug.Log($"{gameObject.name}은 오브젝트풀로 돌려놓습니다. ({Time.time})");
        gameObject.SetActive(false);
        objectPool.ReturnObject(this);
    }
}