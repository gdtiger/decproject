using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuUISystem : MonoBehaviour
{
	public GameObject optionPanel;
	void Start()
	{

	}
	void Update()
	{

	}
	public void OnClickStart()
	{
		SceneManager.LoadScene((int)Scene.GameScene);
	}
	public void OnClickOption()
	{
		optionPanel.SetActive(true);
	}
	public void OnClickExit()
	{
#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
#else
		System.Diagnostics.Process.GetCurrentProcess().Kill();
		Application.Quit();

#endif
		//UnityEditor.EditorApplication.isPlaying = false;
	}
}
