using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerController : MonoBehaviour
{
    //[SerializeField] Rigidbody rb;
    [SerializeField] NavMeshAgent agent;
    [SerializeField] ControlType controlType;
    [SerializeField] GameManager gm;
    [SerializeField] GridManager gridManager;
    [SerializeField] ObjectPoolSCO objectPoolSCO;
    public LayerMask hitLayer;
    public void Start()
    {
        if (gm == null)
        {
            gm = GameManager.Instance;
        }
        if (objectPoolSCO == null)
        {
            objectPoolSCO = GameManager.Instance.objectPool;
        }
        if (gridManager == null)
        {
            gridManager = GameManager.Instance.gridManager;
        }
        if (agent == null)
        {
            agent = GetComponent<NavMeshAgent>();
        }

    }

    private void OnEnable()
    {
        ConnectWithInputManager();
    }

    private void OnDisable()
    {
        DisconnectWithInputManager();
    }

    public void Move() {
        Ray cast = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(cast, out RaycastHit hit, hitLayer))
        {
            if (controlType == ControlType.FreeMode)
            {
                Debug.Log($"{gameObject.name}은 {hit.point}으로 이동합니다.");
                agent.destination = hit.point;
            }
            else
            {
                Vector3 tilePos = Vector3.zero;
                if (gridManager.GetNodePosition(hit.point, ref tilePos))
                {
                    agent.destination = tilePos;
                    Debug.Log($"{hit.point}은  {tilePos}로 변환되어 이동합니다.");
                }
                else
                {

                    Debug.Log($"{gameObject.name}은  {hit.point} 해당위치로 이동할수 없습니다.");

                }
            }
        }// RayCast
       

    }

    public void Fire() {
       var temp = objectPoolSCO.RequestObject(PrefabID.Bullet, transform.position,Quaternion.Euler(transform.forward));
    }

    public void ConnectWithInputManager() {
        InputManager.Instance.ClickButtonA += Move;
        InputManager.Instance.ClickButtonB += Fire;
    }

    public void DisconnectWithInputManager()
    {
        InputManager.Instance.ClickButtonA -= Move;
        InputManager.Instance.ClickButtonB -= Fire;
    }
}
