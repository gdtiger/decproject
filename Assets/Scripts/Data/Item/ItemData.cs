using UnityEngine;

//[CustomEditor(typeof(/*ClassYouAreBuildingAnEditorFor*/))]
[System.Serializable]
public class ItemData : OriginData
{

    public int star = 1;
    public Sprite Image;
    public string imageID;
    public ItemType itemType;

    [Header("Main Status")]

    [Header("HP")]
    public int hp; //
    public int hpLeveling;

    [Header("Damage")]
    public int damage; //weapon
    public int damageLeveling;

    [Header("Defence")]
    public int def;
    public int defLeveling;

    [Header("Element Mastery")]
    public int elementMastery;
    public int elementMasteryLeveling;

    [Header("Element Resistance")]
    public int elementResistance; //
    public int elementResistanceLeveling;
}
