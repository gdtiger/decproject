using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{

    #region instance
    static InputManager instance = null;


    //public List<int> ints;
    //public Dictionary<int, int> dics;

    //[Button]
    //public void CreateDic()
    //{
    //    dics = new Dictionary<int, int>();
    //    dics.Add(1, 1);
    //}

    //[Button]
    //public void TestDic()
    //{
    //    if (dics != null)
    //    {
    //        if (dics.ContainsKey(1))
    //        {

    //            Debug.Log("키값 1이 존재함");

    //        }
    //        else
    //        {
    //            Debug.Log("키값 1이 존재하지않음");
    //        }
    //    }
    //    else
    //    {
    //        Debug.Log("딕셔러리가 널임");
    //    }

    //}

    public static InputManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<InputManager>();
            }
            return instance;
        }
    }
    #endregion


    public Action ClickButtonA;
    public Action ClickButtonB;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ClickButtonA?.Invoke();
        }

        if (Input.GetMouseButtonDown(1))
        {
            ClickButtonB?.Invoke();
        }
    }
}
