using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class SoundManager : MonoBehaviour
{
    #region instance
    private static SoundManager instace;
    public static SoundManager Instance
    {
        get
        {
            if (instace == null)
            {
                instace = FindObjectOfType<SoundManager>();
            }
            return instace;
        }
    }
    #endregion

    private AudioSource bgmPlayer;
    private AudioSource sfxPlayer;

    public float masterVolBGM = 1f;
    public float masterVolSFX = 1f;


    [SerializeField]
    private AudioClip mainBGMClip;
    [SerializeField]
    private AudioClip fieldBGMClip;
    [SerializeField]
    private AudioClip bossBGMClip;

    [SerializeField]
    private AudioClip[] sfxAudioClips;
    [SerializeField]
    private Dictionary<string, AudioClip> sfxClipsDic = new Dictionary<string, AudioClip>();

    private void Awake()
    {
        if (Instance != this)
        {
            Destroy(this.gameObject);
        }
        //DontDestroyOnLoad(this.gameObject);
        bgmPlayer = GameObject.Find("BGMSoundPlayer").GetComponent<AudioSource>();
        sfxPlayer = GameObject.Find("SFXSoundPlayer").GetComponent<AudioSource>();

        foreach (AudioClip audioClip in sfxAudioClips)
        {
            sfxClipsDic.Add(audioClip.name, audioClip);
        }
    }

    public void PlaySFXSound(string name, float volume = 1f)
    {
        if (!sfxClipsDic.ContainsKey(name))
        {
            Debug.Log(name + "는 없는 사운드 입니다 ");
            return;
        }
        sfxPlayer.PlayOneShot(sfxClipsDic[name], volume * masterVolSFX);
    }

    public void PlayBGMSound(string name, float volum = 1f)
    {
        bgmPlayer.loop = true;
        bgmPlayer.volume = volum * masterVolBGM;

        if (bgmPlayer.isPlaying)
        {
            bgmPlayer.Stop();
        }

        if (name == "MainScene")
        {

            bgmPlayer.clip = mainBGMClip;
            bgmPlayer.Play();
        }
        if (name == "FieldScene")
        {

            bgmPlayer.clip = fieldBGMClip;
            bgmPlayer.Play();
        }
        if (name == "RaidScene")
        {

            bgmPlayer.clip = bossBGMClip;
            bgmPlayer.Play();
        }
    }
}