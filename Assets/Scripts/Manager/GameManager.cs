﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public InputManager inputManager;
    public SoundManager soundManager;
    public GridManager gridManager;

    public ObjectPoolSCO objectPool;

    #region instance
    static GameManager instance = null;

    public static GameManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<GameManager>();
            }
            return instance;
        }
    }
    #endregion
    private void Start()
    {
        objectPool.Initiate();
    }

    public void Update()
    {

    }


}
