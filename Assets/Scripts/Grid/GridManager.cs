using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridManager : MonoBehaviour
{
    public Transform controlPoint;
    public Vector2 x;
    public Vector2 y;
    public Vector2 z;

    public LayerMask walkable;
    public LayerMask obstancle;

    public float gridSize = 1;
    public float gridSizeDisplay = 0.9f;
    public float aboveThanHitPoint = 0.2f;


    public float range = 0.00001f;
    public float controlY;

    public StageGridInfomation stageGridInfomation;

    #region instance
    static GridManager instance = null;

    public static GridManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<GridManager>();
            }
            return instance;
        }
    }
    #endregion

    public bool GetNodePosition(Vector3 pos, ref Vector3 targetPos)
    {
        var temp = new Vector2Int(Mathf.RoundToInt(pos.x), Mathf.RoundToInt(pos.z));
        if (stageGridInfomation.gridNode.NodeNullable(temp.x, temp.y))
        {
            var node = stageGridInfomation.gridNode[temp.x, temp.y];
            targetPos = node.position;
            return true;
        }
        else
        {
            return false;
        }
    }



    [Header("Gizmo")]
    public float gizmoPointSphereSize = 0.2f;
    public float gizmoPlaneHeight = 0.2f;
    public bool showLT;
    public bool showLB;
    public bool showRT;
    public bool showRB;

    public Color colorLR;
    public Color colorRL;
    public Color colorTB;
    public Color colorBT;

    public Color colorHillLR;
    public Color colorHillRL;
    public Color colorHillTB;
    public Color colorHillBT;

    public Mesh mesh;
    public bool showMesh;
    public Vector3 rotateOffset;
    public float gizmoMeshSize;

    private void Start()
    {
        CreateGridMap();
    }

    public static bool Approximately(float a, float b, float range = 1E-06f)
    {
        return (double)Mathf.Abs(b - a) < (double)Mathf.Max(range * Mathf.Max(Mathf.Abs(a), Mathf.Abs(b)), Mathf.Epsilon * 8f);
    }

    [Button]
    public void CreateGridMap()
    {
        if (stageGridInfomation == null)
        {
            Debug.LogError($"ScriptableObject를 넣어주세요");
            return;
        }
        float xL = transform.position.x - x.x;
        float xR = transform.position.x + x.y;

        int xCount = (int)((x.y - x.x) / gridSize);
        int zCount = (int)((z.y - z.x) / gridSize);
        stageGridInfomation.gridNode = new GridNode(xCount, zCount);

        for (int i = 0; i < xCount; i++)
        {
            for (int j = 0; j < zCount; j++)
            {
                RaycastHit hitCenter;
                RaycastHit hitLT;
                RaycastHit hitLB;
                RaycastHit hitRT;
                RaycastHit hitRB;


                float xSize = (x.x + gridSize * i);
                float zSize = (z.x + gridSize * j);
                //Center
                if (Physics.Raycast(new Vector3(xSize, y.y + 10, zSize), transform.TransformDirection(Vector3.down), out hitCenter, Mathf.Infinity, walkable)
                    //Left-Top
                    && Physics.Raycast(new Vector3(xSize - gridSize * gridSizeDisplay, y.y + 10, zSize - gridSize * gridSizeDisplay), transform.TransformDirection(Vector3.down), out hitLT, Mathf.Infinity, walkable)
                    //Left-Bottom
                    && Physics.Raycast(new Vector3(xSize - gridSize * gridSizeDisplay, y.y + 10, zSize + gridSize * gridSizeDisplay), transform.TransformDirection(Vector3.down), out hitLB, Mathf.Infinity, walkable)
                    //Right-Top
                    && Physics.Raycast(new Vector3(xSize + gridSize * gridSizeDisplay, y.y + 10, zSize - gridSize * gridSizeDisplay), transform.TransformDirection(Vector3.down), out hitRT, Mathf.Infinity, walkable)
                    //Right-Bottom
                    && Physics.Raycast(new Vector3(xSize + gridSize * gridSizeDisplay, y.y + 10, zSize + gridSize * gridSizeDisplay), transform.TransformDirection(Vector3.down), out hitRB, Mathf.Infinity, walkable)
                    )
                {
                    stageGridInfomation.gridNode.AddNode(i, j, new Node(new Vector2Int(i, j), hitCenter.point, Quaternion.identity));
                    stageGridInfomation.gridNode[i, j].positionLT = hitLT.point;
                    stageGridInfomation.gridNode[i, j].positionLB = hitLB.point;
                    stageGridInfomation.gridNode[i, j].positionRT = hitRT.point;
                    stageGridInfomation.gridNode[i, j].positionRB = hitRB.point;

                    //평면
                    if (Approximately(hitLT.point.y, hitLB.point.y, range)
                        && Approximately(hitLT.point.y, hitRT.point.y, range)
                        && Approximately(hitLT.point.y, hitRB.point.y, range)
                        && Approximately(hitRT.point.y, hitLB.point.y, range)
                        && Approximately(hitRT.point.y, hitRB.point.y, range)
                        && Approximately(hitLB.point.y, hitRB.point.y, range))
                    {
                        stageGridInfomation.gridNode[i, j].tileType = TileType.Moveable_Plane;
                    }
                    //경사로 L -> R
                    else if (Approximately(hitLT.point.y, hitLB.point.y, range) && (hitLT.point.y < hitRT.point.y))
                    {
                        //stageGridInfomation.gridNode[i, j].tileType = TileType.Moveable_Slope_LR;
                        stageGridInfomation.gridNode[i, j].position.y = (hitLT.point.y + hitRT.point.y) * 0.5f;
                        float height = hitRT.point.y - stageGridInfomation.gridNode[i, j].position.y;


                        if (height > 0.6f)
                        {
                            var temp = stageGridInfomation.gridNode[i, j].rotation.eulerAngles;
                            temp.z = 90;
                            stageGridInfomation.gridNode[i, j].rotation = Quaternion.Euler(temp);
                            stageGridInfomation.gridNode[i, j].position.y += temp.x * controlY;
                            stageGridInfomation.gridNode[i, j].tileType = TileType.Moveable_Hill_LR;
                        }
                        else
                        {
                            float slope = (hitRT.point - hitLT.point).magnitude * 0.5f;
                            var temp = stageGridInfomation.gridNode[i, j].rotation.eulerAngles;
                            temp.z += Mathf.Rad2Deg * Mathf.Asin(height / slope);
                            //temp.y += 180;
                            stageGridInfomation.gridNode[i, j].rotation = Quaternion.Euler(temp);
                            stageGridInfomation.gridNode[i, j].position.y -= temp.z * controlY;
                            stageGridInfomation.gridNode[i, j].tileType = TileType.Moveable_Slope_LR;
                        }

                    }
                    //경사로 R -> L
                    else if (Approximately(hitLT.point.y, hitLB.point.y, range) && (hitLT.point.y > hitRT.point.y))
                    {
                        //stageGridInfomation.gridNode[i, j].tileType = TileType.Moveable_Slope_RL;
                        stageGridInfomation.gridNode[i, j].position.y = (hitLT.point.y + hitRT.point.y) * 0.5f;
                        float height = hitLT.point.y - stageGridInfomation.gridNode[i, j].position.y;


                        if (height > 0.6f)
                        {
                            var temp = stageGridInfomation.gridNode[i, j].rotation.eulerAngles;
                            temp.z = 90;
                            stageGridInfomation.gridNode[i, j].rotation = Quaternion.Euler(temp);
                            stageGridInfomation.gridNode[i, j].position.y += temp.x * controlY;
                            stageGridInfomation.gridNode[i, j].tileType = TileType.Moveable_Hill_RL;
                        }
                        else
                        {
                            float slope = (hitLT.point - hitRT.point).magnitude * 0.5f;
                            var temp = stageGridInfomation.gridNode[i, j].rotation.eulerAngles;
                            temp.z -= Mathf.Rad2Deg * Mathf.Asin(height / slope);
                            stageGridInfomation.gridNode[i, j].rotation = Quaternion.Euler(temp);
                            stageGridInfomation.gridNode[i, j].position.y += temp.z * controlY;
                            stageGridInfomation.gridNode[i, j].tileType = TileType.Moveable_Slope_RL;
                        }
                    }
                    //경사로 B -> T
                    else if (Approximately(hitLT.point.y, hitRT.point.y, range) && (hitLT.point.y > hitLB.point.y))
                    {
                        //stageGridInfomation.gridNode[i, j].tileType = TileType.Moveable_Slope_BT;
                        stageGridInfomation.gridNode[i, j].position.y = (hitLT.point.y + hitLB.point.y) * 0.5f;
                        float height = hitLT.point.y - stageGridInfomation.gridNode[i, j].position.y;
                        if (height > 0.6f)
                        {
                            var temp = stageGridInfomation.gridNode[i, j].rotation.eulerAngles;
                            temp.x = 90;
                            stageGridInfomation.gridNode[i, j].rotation = Quaternion.Euler(temp);
                            stageGridInfomation.gridNode[i, j].position.y += temp.x * controlY;
                            stageGridInfomation.gridNode[i, j].tileType = TileType.Moveable_Hill_BT;
                        }
                        else
                        {
                            float slope = (hitLT.point - hitLB.point).magnitude * 0.5f;
                            var temp = stageGridInfomation.gridNode[i, j].rotation.eulerAngles;
                            temp.x += Mathf.Rad2Deg * Mathf.Asin(height / slope);
                            stageGridInfomation.gridNode[i, j].rotation = Quaternion.Euler(temp);
                            stageGridInfomation.gridNode[i, j].position.y -= temp.x * controlY;
                            stageGridInfomation.gridNode[i, j].tileType = TileType.Moveable_Slope_BT;
                        }
                    }
                    //경사로 T -> B
                    else if (Approximately(hitLT.point.y, hitRT.point.y, range) && (hitLT.point.y < hitLB.point.y))
                    {
                        stageGridInfomation.gridNode[i, j].position.y = (hitLT.point.y + hitLB.point.y) * 0.5f;
                        float height = hitLB.point.y - stageGridInfomation.gridNode[i, j].position.y;
                        if (height > 0.6f)
                        {
                            var temp = stageGridInfomation.gridNode[i, j].rotation.eulerAngles;
                            temp.x = 90;
                            stageGridInfomation.gridNode[i, j].rotation = Quaternion.Euler(temp);
                            stageGridInfomation.gridNode[i, j].position.y += temp.x * controlY;
                            stageGridInfomation.gridNode[i, j].tileType = TileType.Moveable_Hill_TB;
                        }
                        else
                        {
                            float slope = (hitLB.point - hitLT.point).magnitude * 0.5f;
                            var temp = stageGridInfomation.gridNode[i, j].rotation.eulerAngles;
                            temp.x -= Mathf.Rad2Deg * Mathf.Asin(height / slope);
                            stageGridInfomation.gridNode[i, j].rotation = Quaternion.Euler(temp);
                            stageGridInfomation.gridNode[i, j].position.y += temp.x * controlY;
                            stageGridInfomation.gridNode[i, j].tileType = TileType.Moveable_Slope_TB;
                        }
                       


                        //Debug.Log($"({i}, {j}) :{height},, {slope},, {temp},, { Mathf.Asin(height / slope)},, {Mathf.Rad2Deg * Mathf.Asin(height / slope)},, { stageGridInfomation.gridNode[i, j].rotation} ");

                    }
                    //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
                }
            }


        }
        stageGridInfomation.gridNode.ConnectNode();
    }

    private void OnDrawGizmos()
    {
        if (stageGridInfomation == null)
        {
            return;
        }
        Gizmos.color = Color.red;

        float xL = transform.position.x - x.x;
        float xR = transform.position.x + x.y;
        float xCenter = (xL + xR) * 0.5f;
        float xRange = (xR - xL);

        float yL = transform.position.y - y.x;
        float yR = transform.position.y + y.y;
        float yCenter = (yL + yR) * 0.5f;
        float yRange = (yR - yL);

        float zL = transform.position.z - z.x;
        float zR = transform.position.z + z.y;
        float zCenter = (zL + zR) * 0.5f;
        float zRange = (zR - zL);
        Gizmos.DrawWireCube(new Vector3(xCenter, yCenter, zCenter), new Vector3(xRange, yRange, zRange));
        if (stageGridInfomation.gridNode != null)
        {
            for (int i = 0; i < stageGridInfomation.gridNode.GetLength(0); i++)
            {
                for (int j = 0; j < stageGridInfomation.gridNode.GetLength(1); j++)
                {
                    if (stageGridInfomation.gridNode[i, j] != null && stageGridInfomation.gridNode[i, j].tileType != TileType.Unmoveable)
                    {
                        Gizmos.color = Color.blue;
                        switch (stageGridInfomation.gridNode[i, j].tileType)
                        {
                            case TileType.Moveable_Slope_LR:
                                Gizmos.color = colorLR;
                                break;
                            case TileType.Moveable_Slope_RL:
                                Gizmos.color = colorRL;
                                break;
                            case TileType.Moveable_Slope_TB:
                                Gizmos.color = colorTB;
                                break;
                            case TileType.Moveable_Slope_BT:
                                Gizmos.color = colorBT;
                                break;
                            case TileType.Moveable_Hill_LR:
                                Gizmos.color = colorHillLR;
                                break;
                            case TileType.Moveable_Hill_TB:
                                Gizmos.color = colorHillTB;
                                break;
                            case TileType.Moveable_Hill_RL:
                                Gizmos.color = colorHillRL;
                                break;
                            case TileType.Moveable_Hill_BT:
                                Gizmos.color = colorHillBT;
                                break;
                        }

                        if (mesh != null && showMesh)
                        {
                            Gizmos.DrawWireMesh(mesh, stageGridInfomation.gridNode[i, j].position + new Vector3(0, aboveThanHitPoint, 0), Quaternion.Euler(stageGridInfomation.gridNode[i, j].rotation.eulerAngles + rotateOffset), Vector3.one * gizmoMeshSize);
                        }
                        else
                        {
                            Gizmos.DrawWireCube(stageGridInfomation.gridNode[i, j].position + new Vector3(0, aboveThanHitPoint, 0), new Vector3(gridSize, gizmoPlaneHeight, gridSize));
                        }

                        if (showLT)
                        {
                            Gizmos.color = colorLR;
                            Gizmos.DrawWireSphere(stageGridInfomation.gridNode[i, j].positionLT + new Vector3(0, aboveThanHitPoint, 0), gizmoPointSphereSize + 0.1f);
                        }
                        if (showLB)
                        {
                            Gizmos.color = colorRL;
                            Gizmos.DrawWireSphere(stageGridInfomation.gridNode[i, j].positionLB + new Vector3(0, aboveThanHitPoint, 0), gizmoPointSphereSize + 0.06f);
                        }
                        if (showRT)
                        {
                            Gizmos.color = colorTB;
                            Gizmos.DrawWireSphere(stageGridInfomation.gridNode[i, j].positionRT + new Vector3(0, aboveThanHitPoint, 0), gizmoPointSphereSize + 0.03f);
                        }
                        if (showRB)
                        {
                            Gizmos.color = colorBT;
                            Gizmos.DrawWireSphere(stageGridInfomation.gridNode[i, j].positionRB + new Vector3(0, aboveThanHitPoint, 0), gizmoPointSphereSize);
                        }



                    }

                    //Debug.Log($"{tiles.GetLength(1)}");
                }
            }
        }

    }
}

