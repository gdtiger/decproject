﻿
[System.Flags]
public enum ItemType {

    //Null = 0, 
    //Weapon = 1<<0, 
    //Armor = 1<<1, 
    //Artifect = 1<<2,

    //Two_Hand_Sword = 1<<10,
    //Bow = 1<<11,

    //Chest = 1<<20,
    //Pants = 1<<21,
    //helmet = 1<<22,

    Null = 0,
    Weapon = 1 << 0,
    Armor = 1 << 1,
    Artifect = 1 << 2,

    Two_Hand_Sword = 1 << 10,
    Bow = 1 << 11,

    Chest = 1 << 20,
    Pants = 1 << 21,
    helmet = 1 << 22,
}


public enum DirEight { 
T,
LT,
L,
LB,
B,
RB,
R,
RT

}



public enum OrderItemMethod{ 
    Name,
    ID
}

public enum ControlType {
    FreeMode,
    BattleMode
}